resource "hcloud_network" "private-net" {
  name     = "private-net"
  ip_range = "10.0.0.0/8"
}
resource "hcloud_network_subnet" "private-subnet" {
  network_id   = hcloud_network.private-net.id
  type         = "cloud"
  network_zone = "eu-central"
  ip_range     = "10.0.1.0/24"
}
