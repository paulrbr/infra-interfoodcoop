# SSH access

## Public keys

This directory holds all known public keys to give access to some people (or service account if needed).

## SSH client config

In order to connect to servers it is recommended to configure your local ssh client via the `~/.ssh/config` file.

For instance, for the `intercoop-cloud` environment, this is what I have in my file:

```
Host nuage.interfoodcoop.net rocket.interfoodcoop.net
  User root
  IdentitiesOnly yes
  IdentityFile  ~/.ssh/id_rsa_path_to_my_secret_key
```

This config will automatically try to connect with the `root` user, and use the private key `~/.ssh/id_rsa_path_to_my_secret_key` to connect to either `rocket.interfoodcoop.net` server (via `> ssh rocket.interfoodcoop.net`) or `nuage.interfoodcoop.net` (via `> ssh nuage.interfoodcoop.net`)
